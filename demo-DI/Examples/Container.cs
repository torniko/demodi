﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo_DI.Examples
{
    class Container : IoContainer
    {
        List<object> shemnaxveli1 = new List<object>();
        List<object> shemnaxveli2 = new List<object>();

        void IoContainer.Register<T>()
        {
            shemnaxveli1.Add(typeof(T));
        }

        void IoContainer.Register<T, R>()
        {
            shemnaxveli1.Add(typeof(T));
            shemnaxveli2.Add(typeof(R));
        }


        // მინდა რომ ორ სხვადასხვა კლასში შევიტანო ტაიპები

        // რესოლვში მერე ამ ტაიპების ობიექტები გავაკეთთო

        //მაგრამ ვერ ვაკეთებ

        void IoContainer.Register<T>(Func<T> factory)
        {
            var klasi = Activator.CreateInstance(shemnaxveli1.FirstOrDefault());
        }

        T IoContainer.Resolve<T>()
        {
            throw new NotImplementedException();
        }
    }
}
